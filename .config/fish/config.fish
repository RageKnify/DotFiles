starship init fish | source
env _ZO_ECHO=1 zoxide init fish | source
# Ctrl+T file-widget
# Ctrl+R history-widget
# Alt+D cd-widget
fzf_key_bindings
